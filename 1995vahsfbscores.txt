1995-09-01|Atlantic Shores Christian|21|Isle of Wight|0
1995-09-01|Catholic|21|Christchurch|0
1995-09-01|Churchland|41|Norcom|6
1995-09-01|Deep Creek|20|Booker T. Washington|7
1995-09-01|First Colonial|16|Bayside|10
1995-09-01|Granby|6|Wilson|0
1995-09-01|Greenbrier Christian|38|Isle of Wight|8
1995-09-01|Indian River|13|Norview|6
1995-09-01|Lake Taylor|21|Salem|7
1995-09-01|Lakeland|21|Nansemond River|14
1995-09-01|Ocean Lakes|30|Princess Anne|27
1995-09-01|Oscar Smith|14|Maury|0
1995-09-01|Tallwood|28|Great Bridge|16
1995-09-08|Atlantic Shores Christian|35|Nansemond-Suffolk|12
1995-09-08|Booker T. Washington|10|Oscar Smith|8
1995-09-08|Central(L)|15|Lakeland|14
1995-09-08|Cox|21|Ocean Lakes|20
1995-09-08|Green Run|39|Maury|6
1995-09-08|Greenbrier Christian|34|Catholic|0
1995-09-08|Kellam|9|Norview|8
1995-09-08|Kempsville|26|Great Bridge|6
1995-09-08|Lake Taylor|21|Wilson|20
1995-09-08|Nansemond River|20|Brunswick|0
1995-09-08|Norfolk Academy|46|Hampton Roads Academy|20
1995-09-08|Tallwood|40|Princess Anne|0
1995-09-08|Western Branch|28|Granby|20
1995-09-15|Booker T. Washington|19|Lake Taylor|0
1995-09-15|Churchland|36|Great Bridge|20
1995-09-15|Colonial Beach|32|Greenbrier Christian|7
1995-09-15|Deep Creek|42|Wilson|0
1995-09-15|Granby|26|Cox|0
1995-09-15|Green Run|37|Bayside|0
1995-09-15|Greensville|18|Lakeland|14
1995-09-15|Indian River|47|Maury|6
1995-09-15|Kempsville|34|First Colonial|19
1995-09-15|Matoaca|21|Nansemond River|8
1995-09-15|Norfolk Academy|28|Atlantic Shores Christian|15
1995-09-15|Norview|21|Ocean Lakes|20
1995-09-15|Poquoson|14|Nansemond-Suffolk|6
1995-09-15|Salem|30|Kellam|14
1995-09-15|Western Branch|20|Norcom|13
1995-09-22|Atlantic Shores Christian|21|Catholic|13
1995-09-22|Booker T. Washington|14|Bayside|6
1995-09-22|Collegiate|50|Nansemond-Suffolk|0
1995-09-22|Cox|26|Kellam|17
1995-09-22|Deep Creek|17|Granby|6
1995-09-22|Gates County|26|Nansemond-Suffolk|14
1995-09-22|Great Bridge|25|Wilson|0
1995-09-22|Green Run|34|Ocean Lakes|6
1995-09-22|Indian River|20|Lake Taylor|0
1995-09-22|Kempsville|37|Princess Anne|12
1995-09-22|Maury|7|First Colonial|0
1995-09-22|Nansemond River|23|Franklin|8
1995-09-22|Norfolk Academy|28|St. Christopher's|0
1995-09-22|Oscar Smith|14|Norcom|6
1995-09-22|Salem|3|Tallwood|0
1995-09-22|Southampton|20|Lakeland|14
1995-09-22|Western Branch|38|Churchland|13
1995-09-29|Bayside|17|Princess Anne|15
1995-09-29|Benedictine|40|Catholic|0
1995-09-29|Deep Creek|29|Churchland|21
1995-09-29|Granby|20|Oscar Smith|6
1995-09-29|Green Run|48|First Colonial|17
1995-09-29|Indian River|46|Wilson|2
1995-09-29|Kempsville|26|Salem|23
1995-09-29|Lakeland|49|Poquoson|0
1995-09-29|Norcom|13|Great Bridge|12
1995-09-29|Norfolk Academy|40|Greenbrier Christian|14
1995-09-29|Norview|28|Lake Taylor|23
1995-09-29|Ocean Lakes|41|Kellam|14
1995-09-29|Riverdale|13|Atlantic Shores Christian|12
1995-09-29|St. Christopher's|43|Nansemond-Suffolk|7
1995-09-29|Southampton|35|Nansemond River|20
1995-09-29|Tallwood|38|Cox|8
1995-09-29|Western Branch|33|Maury|6
1995-10-06|Atlantic Shores Christian|20|Lynchburg Christian|7
1995-10-06|Booker T. Washington|17|Granby|12
1995-10-06|Churchland|14|Oscar Smith|8
1995-10-06|Deep Creek|29|Lake Taylor|0
1995-10-06|Green Run|51|Princess Anne|13
1995-10-06|Greenbrier Christian|26|Hampton Roads Academy|8
1995-10-06|Indian River|26|Norcom|20
1995-10-06|Kempsville|31|Cox|20
1995-10-06|Lakeland|47|Smithfield|6
1995-10-06|Nansemond River|22|Poquoson|3
1995-10-06|Nansemond-Suffolk|8|Catholic|0
1995-10-06|Norview|40|Great Bridge|30
1995-10-06|Ocean Lakes|22|First Colonial|14
1995-10-06|Salem|35|Bayside|28
1995-10-06|Tallwood|48|Kellam|0
1995-10-06|Western Branch|34|Wilson|13
1995-10-13|Atlantic Shores Christian|27|Chincoteague|12
1995-10-13|Booker T. Washington|19|Norcom|6
1995-10-13|Catholic|20|Hampton Roads Academy|13
1995-10-13|Churchland|15|Lake Taylor|13
1995-10-13|Cox|7|Bayside|6
1995-10-13|Deep Creek|34|Indian River|20
1995-10-13|Green Run|42|Salem|15
1995-10-13|Kempsville|28|Kellam|20
1995-10-13|Lakeland|45|Franklin|8
1995-10-13|Maury|24|Norview|14
1995-10-13|Nansemond River|47|Smithfield|6
1995-10-13|Norfolk Academy|41|Nansemond-Suffolk|0
1995-10-13|Oscar Smith|22|Wilson|3
1995-10-13|Princess Anne|18|First Colonial|3
1995-10-13|Quantico|66|Greenbrier Christian|19
1995-10-13|Tallwood|43|Ocean Lakes|6
1995-10-13|Western Branch|43|Great Bridge|14
1995-10-20|Atlantic Shores Christian|29|Colonial Beach|12
1995-10-20|Bayside|46|Kellam|17
1995-10-20|Booker T. Washington|21|Churchland|10
1995-10-20|Deep Creek|28|Oscar Smith|12
1995-10-20|Fuqua|42|Catholic|18
1995-10-20|Green Run|35|Cox|3
1995-10-20|Huguenot Academy|22|Greenbrier Christian|7
1995-10-20|Indian River|7|Great Bridge|0
1995-10-20|Lake Taylor|22|Western Branch|14
1995-10-20|Lakeland|42|Bruton|14
1995-10-20|Nansemond River|13|York|7
1995-10-20|Nansemond-Suffolk|15|Hampton Roads Academy|7
1995-10-20|Norcom|16|Wilson|6
1995-10-20|Norview|33|Granby|14
1995-10-20|Ocean Lakes|18|Kempsville|14
1995-10-20|Princess Anne|31|Maury|24
1995-10-20|Salem|22|First Colonial|0
1995-10-20|Tallwood|16|Kempsville|0
1995-10-20|Woodberry Forest|28|Norfolk Academy|14
1995-10-27|Atlantic Shores Christian|36|Hampton Roads Academy|0
1995-10-27|Booker T. Washington|20|Maury|0
1995-10-27|Broadwater|8|Greenbrier Christian|6
1995-10-27|Churchland|14|Norview|6
1995-10-27|Collegiate|12|Norfolk Academy|9
1995-10-27|Cox|28|First Colonial|7
1995-10-27|Deep Creek|38|Norcom|6
1995-10-27|Granby|14|Lake Taylor|0
1995-10-27|Green Run|57|Kellam|0
1995-10-27|Lakeland|48|York|10
1995-10-27|Nansemond River|20|Bruton|14
1995-10-27|Northampton|33|Nansemond-Suffolk|0
1995-10-27|Oscar Smith|25|Great Bridge|6
1995-10-27|Salem|34|Princess Anne|20
1995-10-27|Tallwood|20|Bayside|7
1995-10-27|Western Branch|21|Indian River|0
1995-11-03|Atlantic Shores Christian|21|Blue Ridge Academy|11
1995-11-03|Booker T. Washington|21|Wilson|6
1995-11-03|Collegiate|47|Catholic|14
1995-11-03|Deep Creek|34|Great Bridge|0
1995-11-03|First Colonial|28|Kellam|0
1995-11-03|Fork Union|32|Norfolk Academy|3
1995-11-03|Granby|20|Maury|0
1995-11-03|Green Run|28|Tallwood|7
1995-11-03|Indian River|21|Churchland|7
1995-11-03|Kempsville|34|Bayside|21
1995-11-03|Nansemond-Suffolk|14|Greenbrier Christian|0
1995-11-03|Norcom|28|Norview|0
1995-11-03|Princess Anne|34|Cox|14
1995-11-03|Salem|16|Ocean Lakes|0
1995-11-03|Western Branch|32|Oscar Smith|8
1995-11-10|Booker T. Washington|28|Norview|26
1995-11-10|Churchland|40|Wilson|0
1995-11-10|Cox|22|Salem|15
1995-11-10|Deep Creek|38|Western Branch|0
1995-11-10|Granby|12|Norcom|0
1995-11-10|Green Run|28|Kempsville|14
1995-11-10|Hampton|22|Bethel|12
1995-11-10|Indian River|43|Oscar Smith|14
1995-11-10|Lakeland|34|Nansemond River|20
1995-11-10|Maury|21|Lake Taylor|18
1995-11-10|Norfolk Academy|48|Catholic|8
1995-11-10|Ocean Lakes|34|Bayside|19
1995-11-10|Princess Anne|51|Kellam|14
1995-11-10|Tallwood|42|First Colonial|0
1995-11-17|Deep Creek|20|Kecoughtan|7
1995-11-17|Green Run|34|Kempsville|7
1995-11-17|Hampton|28|Granby|0
1995-11-17|Indian River|27|Tallwood|0
1995-11-25|Hampton|24|Deep Creek|19
1995-11-25|Indian River|14|Green Run|7
1995-12-02|Indian River|31|Lake Braddock|10
1995-12-02|C.D. Hylton|21|Varina|14
1995-12-02|Hampton|21|McLean|0
1995-12-02|E.C. Glass|34|Lee-Davis|32
1995-12-02|Amherst County|34|Grundy|8
1995-12-02|Sherando|28|Nansemond River|7
1995-12-02|Graham|7|William Campbell|6
1995-12-02|King George|43|Matoaca|20
1995-12-02|Powell Valley|28|Giles|6
1995-12-02|Northampton|26|Stuarts Draft|6
1995-12-02|Bath County|36|Appalachia|14
1995-12-02|Strasburg|22|Windsor|13
1995-12-09|Indian River|13|C.D. Hylton|12
1995-12-09|Hampton|35|E.C. Glass|7
1995-12-09|Amherst County|56|Sherando|12
1995-12-09|Graham|23|King George|18
1995-12-09|Powell Valley|36|Northampton|6
1995-12-09|Bath County|30|Strasburg|20
