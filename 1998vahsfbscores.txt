1998-08-29|Heritage(NN)|14|Hampton|8
1998-08-29|Phoebus|10|Warwick|0
1998-08-30|Bethel|35|Gloucester|14
1998-08-30|Menchville|8|Denbigh|3
1998-08-31|Deep Creek|34|Booker T. Washington|8
1998-08-31|First Colonial|14|Maury|0
1998-08-31|Kellam|7|Great Bridge|6
1998-08-31|Granby|36|Cox|0
1998-08-31|Lake Taylor|13|Salem(VB)|0
1998-09-03|Norview|14|Bethel|7
1998-09-03|Western Branch|28|Norcom|14
1998-09-04|Bayside|28|Indian River|6
1998-09-04|Deep Creek|36|Woodrow Wilson|14
1998-09-04|Green Run|25|Maury|14
1998-09-04|Hampton|47|Kecoughtan|0
1998-09-04|Hickory|28|Ocean Lakes|15
1998-09-04|Heritage(NN)|24|Warwick|3
1998-09-04|Kempsville|10|Great Bridge|3
1998-09-04|Nansemond River|13|Lakeland|0
1998-09-04|Oscar Smith|9|Churchland|7
1998-09-04|Princess Anne|15|Granby|14
1998-09-04|Tallwood|17|Lake Taylor|7
1998-09-05|Phoebus|21|Denbigh|6
1998-09-05|Woodside|32|Menchville|0
1998-09-11|Bethel|24|Woodside|7
1998-09-11|First Colonial|28|Kempsville|7
1998-09-11|Green Run|12|Princess Anne|7
1998-09-11|Heritage(NN)|24|Lakeland|0
1998-09-11|Hickory|29|Norcom|0
1998-09-11|Indian River|17|Norview|0
1998-09-11|Maury|20|Great Bridge|19
1998-09-11|Menchville|10|Gloucester|6
1998-09-11|Nansemond River|13|Churchland|12
1998-09-11|Ocean Lakes|7|Kellam|6
1998-09-11|Oscar Smith|13|Granby|6
1998-09-11|Phoebus|10|Kecoughtan|6
1998-09-11|Salem(VB)|14|Bayside|7
1998-09-11|Tallwood|56|Cox|20
1998-09-11|Western Branch|32|Woodrow Wilson|0
1998-09-12|Denbigh|21|Booker T. Washington|0
1998-09-12|Hampton|47|Warwick|7
1998-09-18|Deep Creek|20|Norcom|12
1998-09-18|First Colonial|9|Princess Anne|6
1998-09-18|Cox|28|Bayside|20
1998-09-18|Green Run|13|Kellam|10
1998-09-18|Hampton|9|Denbigh|3
1998-09-18|Heritage(NN)|23|Kecoughtan|0
1998-09-18|Indian River|22|Booker T. Washington|19
1998-09-18|Kempsville|19|Ocean Lakes|7
1998-09-18|Lake Taylor|16|Hickory|10
1998-09-18|Nansemond River|27|Woodrow Wilson|12
1998-09-18|Norview|33|Oscar Smith|24
1998-09-18|Tallwood|23|Salem(VB)|3
1998-09-18|Western Branch|28|Churchland|14
1998-09-19|Bethel|19|Menchville|0
1998-09-19|Phoebus|42|Gloucester|0
1998-09-19|Warwick|14|Lakeland|6
1998-09-24|Woodrow Wilson|28|Maury|25
1998-09-25|Bayside|32|Kempsville|27
1998-09-25|Booker T. Washington|8|Lake Taylor|6
1998-09-25|Churchland|30|Granby|14
1998-09-25|Deep Creek|20|Oscar Smith|3
1998-09-25|Cox|20|Princess Anne|19
1998-09-25|Great Bridge|33|Lakeland|19
1998-09-25|Green Run|28|Ocean Lakes|13
1998-09-25|Nansemond River|31|Indian River|12
1998-09-25|Norview|33|Norcom|6
1998-09-25|Phoebus|27|Bethel|12
1998-09-25|Salem(VB)|21|Kellam|10
1998-09-25|Tallwood|28|First Colonial|0
1998-09-25|Warwick|23|Denbigh|6
1998-09-25|Western Branch|42|Hickory|29
1998-09-26|Hampton|46|Woodside|0
1998-09-26|Heritage(NN)|26|Gloucester|0
1998-10-02|Bayside|42|Green Run|32
1998-10-02|Churchland|21|Booker T. Washington|14
1998-10-02|Deep Creek|20|Hickory|10
1998-10-02|First Colonial|20|Salem(VB)|10
1998-10-02|Granby|20|Lake Taylor|0
1998-10-02|Indian River|18|Great Bridge|17
1998-10-02|Kecoughtan|10|Menchville|7
1998-10-02|Kempsville|35|Princess Anne|0
1998-10-02|Maury|10|Norcom|7
1998-10-02|Nansemond River|17|Oscar Smith|14
1998-10-02|Ocean Lakes|12|Cox|10
1998-10-02|Phoebus|26|Heritage(NN)|6
1998-10-02|Tallwood|31|Kellam|7
1998-10-02|Western Branch|14|Lakeland|12
1998-10-02|Woodside|24|Gloucester|0
1998-10-03|Bethel|20|Denbigh|0
1998-10-03|Norview|22|Woodrow Wilson|0
1998-10-09|Churchland|51|Lake Taylor|29
1998-10-09|Deep Creek|7|Nansemond River|0
1998-10-09|First Colonial|28|Green Run|6
1998-10-09|Kellam|20|Cox|7
1998-10-09|Granby|14|Booker T. Washington|0
1998-10-09|Hampton|21|Bethel|0
1998-10-09|Heritage(NN)|32|Woodside|10
1998-10-09|Hickory|34|Lakeland|28
1998-10-09|Norcom|21|Woodrow Wilson|6
1998-10-09|Norview|6|Maury|2
1998-10-09|Oscar Smith|6|Great Bridge|2
1998-10-09|Salem(VB)|27|Princess Anne|7
1998-10-09|Tallwood|14|Kempsville|7
1998-10-09|Warwick|20|Gloucester|6
1998-10-09|Western Branch|13|Indian River|10
1998-10-10|Denbigh|10|Kecoughtan|7
1998-10-10|Ocean Lakes|21|Bayside|20
1998-10-10|Phoebus|24|Menchville|6
1998-10-16|Deep Creek|47|Lakeland|8
1998-10-16|First Colonial|35|Cox|14
1998-10-16|Granby|24|Norcom|20
1998-10-16|Hampton|49|Gloucester|0
1998-10-16|Heritage(NN)|40|Denbigh|0
1998-10-16|Hickory|29|Indian River|13
1998-10-16|Kempsville|20|Kellam|7
1998-10-16|Lake Taylor|20|Norview|8
1998-10-16|Maury|18|Churchland|16
1998-10-16|Nansemond River|20|Great Bridge|19
1998-10-16|Ocean Lakes|20|Salem(VB)|13
1998-10-16|Princess Anne|16|Bayside|12
1998-10-16|Tallwood|23|Green Run|0
1998-10-16|Western Branch|23|Oscar Smith|3
1998-10-16|Woodrow Wilson|12|Booker T. Washington|0
1998-10-17|Phoebus|34|Woodside|0
1998-10-17|Warwick|23|Kecoughtan|6
1998-10-23|Deep Creek|36|Great Bridge|0
1998-10-23|Denbigh|20|Woodside|7
1998-10-23|First Colonial|21|Bayside|20
1998-10-23|Granby|26|Norview|6
1998-10-23|Green Run|20|Salem(VB)|7
1998-10-23|Hampton|10|Phoebus|7
1998-10-23|Hickory|9|Oscar Smith|6
1998-10-23|Indian River|22|Lakeland|6
1998-10-23|Kempsville|38|Cox|23
1998-10-23|Lake Taylor|19|Woodrow Wilson|0
1998-10-23|Norcom|18|Churchland|14
1998-10-23|Princess Anne|21|Kellam|7
1998-10-23|Tallwood|29|Ocean Lakes|3
1998-10-23|Western Branch|7|Nansemond River|6
1998-10-24|Bethel|21|Kecoughtan|6
1998-10-24|Maury|6|Booker T. Washington|0
1998-10-24|Warwick|24|Menchville|21
1998-10-30|Bayside|19|Kellam|13
1998-10-30|Bethel|22|Warwick|17
1998-10-30|Churchland|28|Norview|8
1998-10-30|Deep Creek|35|Indian River|12
1998-10-30|Cox|34|Green Run|15
1998-10-30|Gloucester|3|Denbigh|0
1998-10-30|Granby|24|Woodrow Wilson|6
1998-10-30|Heritage(NN)|53|Menchville|0
1998-10-30|Kecoughtan|17|Woodside|7
1998-10-30|Maury|6|Lake Taylor|0
1998-10-30|Nansemond River|25|Hickory|17
1998-10-30|Norcom|26|Booker T. Washington|6
1998-10-30|Ocean Lakes|21|First Colonial|19
1998-10-30|Oscar Smith|19|Lakeland|12
1998-10-30|Salem(VB)|7|Kempsville|3
1998-10-30|Tallwood|26|Princess Anne|13
1998-10-30|Western Branch|27|Great Bridge|10
1998-11-06|Booker T. Washington|7|Norview|6
1998-11-06|Churchland|51|Woodrow Wilson|6
1998-11-06|Deep Creek|7|Western Branch|0
1998-11-06|First Colonial|20|Kellam|8
1998-11-06|Granby|32|Maury|21
1998-11-06|Great Bridge|21|Hickory|7
1998-11-06|Hampton|62|Menchville|0
1998-11-06|Indian River|22|Oscar Smith|0
1998-11-06|Kecoughtan|24|Gloucester|13
1998-11-06|Kempsville|35|Green Run|27
1998-11-06|Lake Taylor|26|Norcom|23
1998-11-06|Nansemond River|28|Lakeland|14
1998-11-06|Princess Anne|13|Ocean Lakes|12
1998-11-06|Salem(VB)|26|Cox|21
1998-11-06|Tallwood|35|Bayside|0
1998-11-06|Warwick|22|Woodside|0
1998-11-07|Heritage(NN)|27|Bethel|9
1998-11-13|Hampton|35|Deep Creek|26
1998-11-13|Phoebus|17|Granby|6
1998-11-13|Tallwood|35|Kempsville|15
1998-11-13|Western Branch|20|First Colonial|7
1998-11-13|Atlee|23|Petersburg|20
1998-11-13|Varina|49|Mills Godwin|20
1998-11-13|Centreville|40|Langley|20
1998-11-13|Oakton|22|Lake Braddock|0
1998-11-13|C.D. Hylton|49|Potomac|6
1998-11-13|Gar-Field|6|North Stafford|3
1998-11-13|Huguenot|30|Monacan|22
1998-11-13|Patrick Henry(A)|24|Hopewell|23
1998-11-13|West Potomac|27|Yorktown|21
1998-11-13|Park View(S)|47|Mount Vernon|21
1998-11-13|Amherst County|24|E.C. Glass|21
1998-11-13|Culpeper County|14|William Fleming|13
1998-11-13|Lafayette|52|Spotsylvania|18
1998-11-13|Orange|38|Poquoson|0
1998-11-13|Sherando|24|Spotswood|14
1998-11-13|Liberty(Beal)|28|Charlottesville|14
1998-11-13|Salem|40|Jefferson Forest|0
1998-11-13|Heritage(L)|34|Christiansburg|0
1998-11-13|Martinsville|36|Patrick County|8
1998-11-13|Richlands|24|Lee|12
1998-11-13|Nottoway|58|James Monroe|7
1998-11-13|Matoaca|21|Bruton|18
1998-11-13|Handley|28|Fort Defiance|16
1998-11-13|Waynesboro|17|Broadway|7
1998-11-13|Tunstall|21|Gretna|12
1998-11-13|Rustburg|37|Lord Botetourt|20
1998-11-13|Lebanon|34|Laurel Park|14
1998-11-13|Graham|28|Bassett|7
1998-11-13|New Kent|30|King William|6
1998-11-13|Essex|48|Arcadia|17
1998-11-13|Madison County|47|Central(L)|14
1998-11-13|Powhatan|28|Buffalo Gap|20
1998-11-13|John Battle|31|Floyd County|12
1998-11-13|Giles|29|Grayson|21
1998-11-13|Powell Valley|38|Honaker|19
1998-11-13|Clintwood|35|Haysi|20
1998-11-13|Sussex Central|14|West Point|6
1998-11-13|Surry|32|Charles City|13
1998-11-13|Strasburg|15|Riverheads|7
1998-11-13|Luray|51|George Mason|29
1998-11-13|Covington|31|Pocahontas|6
1998-11-13|Narrows|8|Galax|0
1998-11-13|Appalachia|35|Rye Cove|18
1998-11-13|J.I. Burton|34|Twin Springs|12
1998-11-20|Hampton|28|Phoebus|6
1998-11-20|Tallwood|41|Western Branch|0
1998-11-20|Varina|10|Atlee|0
1998-11-20|Centreville|21|Oakton|19
1998-11-20|C.D. Hylton|46|Gar-Field|15
1998-11-20|Huguenot|33|Patrick Henry(A)|7
1998-11-20|West Potomac|42|Park View(S)|8
1998-11-20|Amherst County|20|Culpeper County|10
1998-11-20|Lafayette|37|Orange|21
1998-11-20|Liberty(Beal)|24|Sherando|0
1998-11-20|Salem|26|Heritage(L)|0
1998-11-20|Martinsville|42|Richlands|13
1998-11-20|Nottoway|26|Matoaca|20
1998-11-20|Handley|20|Waynesboro|13
1998-11-20|Rustburg|14|Tunstall|9
1998-11-20|Graham|41|Lebanon|20
1998-11-20|New Kent|22|Essex|0
1998-11-20|Madison County|21|Powhatan|15
1998-11-20|Giles|30|John Battle|14
1998-11-20|Powell Valley|27|Clintwood|14
1998-11-20|Surry|39|Sussex Central|14
1998-11-20|Strasburg|28|Luray|21
1998-11-20|Covington|41|Narrows|8
1998-11-20|Appalachia|35|Rye Cove|18
1998-11-20|J.I. Burton|34|Appalachia|13
1998-11-28|Varina|31|Tallwood|13
1998-11-28|C.D. Hylton|24|Centreville|7
1998-11-28|Amherst County|26|West Potomac|13
1998-11-28|Hampton|38|Huguenot|8
1998-11-28|Lafayette|21|Liberty(Beal)|7
1998-11-28|Salem|17|Martinsville|0
1998-11-28|Rustburg|42|Graham|7
1998-11-28|Nottoway|18|Handley|13
1998-11-28|Powell Valley|40|Giles|0
1998-11-28|Madison County|44|New Kent|22
1998-11-28|Surry|28|Strasburg|0
1998-11-28|J.I. Burton|25|Covington|13
1998-12-05|C.D. Hylton|21|Varina|13
1998-12-05|Hampton|35|Amherst County|0
1998-12-05|Salem|28|Lafayette|18
1998-12-05|Nottoway|44|Rustburg|27
1998-12-05|Powell Valley|27|Madison County|14
1998-12-05|Surry|46|J.I. Burton|6
