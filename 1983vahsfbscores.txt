1983-09-09|Bethel|17|Green Run|14
1983-09-09|Cox|27|Princess Anne|26
1983-09-09|Cradock|2|Deep Creek|0
1983-09-09|First Colonial|10|Kellam|7
1983-09-09|Indian River|24|Norcom|13
1983-09-09|Kempsville|33|Bayside|13
1983-09-09|Lafayette|20|Granby|0
1983-09-09|Lake Taylor|16|Booker T. Washington|0
1983-09-09|Manor|6|Great Bridge|0
1983-09-09|Maury|21|Norview|17
1983-09-09|Oscar Smith|25|Churchland|6
1983-09-09|Western Branch|6|Woodrow Wilson|0
1983-09-16|Bayside|8|Cox|7
1983-09-16|Booker T. Washington|14|Norview|7
1983-09-16|First Colonial|23|Granby|6
1983-09-16|Great Bridge|19|Princess Anne|7
1983-09-16|Green Run|19|Deep Creek|13
1983-09-16|Indian River|21|Cradock|6
1983-09-16|Kellam|16|Maury|7
1983-09-16|Kempsville|7|Lake Taylor|6
1983-09-16|Manor|10|Western Branch|7
1983-09-16|Norcom|7|Oscar Smith|7
1983-09-16|Woodrow Wilson|19|Churchland|7
1983-09-23|Churchland|6|Western Branch|0
1983-09-23|Ferguson|16|Granby|14
1983-09-23|First Colonial|54|Princess Anne|21
1983-09-23|Great Bridge|17|Oscar Smith|14
1983-09-23|Green Run|27|Bayside|0
1983-09-23|Indian River|10|Deep Creek|7
1983-09-23|Kellam|7|Norview|6
1983-09-23|Lake Taylor|27|Kecoughtan|13
1983-09-23|Maury|21|Cox|20
1983-09-23|Norcom|13|Booker T. Washington|8
1983-09-23|Woodrow Wilson|6|Manor|0
1983-09-30|Deep Creek|6|Churchland|2
1983-09-30|First Colonial|28|Bayside|24
1983-09-30|Granby|14|Menchville|13
1983-09-30|Great Bridge|35|Norcom|0
1983-09-30|Green Run|34|Cox|7
1983-09-30|Indian River|20|Princess Anne|12
1983-09-30|Kempsville|24|Kellam|0
1983-09-30|Lake Taylor|17|Maury|17
1983-09-30|Manor|12|Booker T. Washington|8
1983-09-30|Oscar Smith|7|Norview|6
1983-09-30|Western Branch|14|Cradock|0
1983-10-07|Bayside|14|Kellam|7
1983-10-07|Deep Creek|13|Western Branch|0
1983-10-07|Granby|14|Booker T. Washington|9
1983-10-07|Great Bridge|33|Indian River|3
1983-10-07|Green Run|24|Princess Anne|13
1983-10-07|Kempsville|10|First Colonial|7
1983-10-07|Lake Taylor|41|Cox|8
1983-10-07|Manor|21|Churchland|0
1983-10-07|Oscar Smith|6|Cradock|2
1983-10-07|Norcom|26|Woodrow Wilson|6
1983-10-07|Norview|20|Kecoughtan|14
1983-10-14|Bayside|6|Booker T. Washington|0
1983-10-14|Cradock|15|Churchland|13
1983-10-14|First Colonial|28|Cox|21
1983-10-14|Great Bridge|32|Deep Creek|6
1983-10-14|Green Run|27|Kellam|9
1983-10-14|Indian River|12|Woodrow Wilson|8
1983-10-14|Kempsville|27|Maury|16
1983-10-14|Lake Taylor|36|Granby|14
1983-10-14|Manor|14|Norcom|7
1983-10-14|Norview|20|Princess Anne|0
1983-10-14|Western Branch|23|Oscar Smith|20
1983-10-21|Cradock|7|Great Bridge|0
1983-10-21|Deep Creek|6|Woodrow Wilson|2
1983-10-21|First Colonial|21|Booker T. Washington|6
1983-10-21|Green Run|29|Granby|9
1983-10-21|Indian River|25|Western Branch|0
1983-10-21|Kellam|14|Princess Anne|6
1983-10-21|Kempsville|37|Cox|6
1983-10-21|Maury|23|Bayside|0
1983-10-21|Norcom|34|Churchland|13
1983-10-21|Norview|10|Lake Taylor|7
1983-10-21|Oscar Smith|10|Manor|0
1983-10-28|Cradock|0|Manor|0
1983-10-28|Deep Creek|19|Norcom|6
1983-10-28|Granby|20|Norview|15
1983-10-28|Great Bridge|26|Western Branch|14
1983-10-28|Green Run|21|First Colonial|13
1983-10-28|Indian River|33|Churchland|6
1983-10-28|Kellam|11|Cox|7
1983-10-28|Kempsville|49|Princess Anne|14
1983-10-28|Lake Taylor|40|Bayside|6
1983-10-28|Maury|12|Booker T. Washington|8
1983-10-28|Oscar Smith|13|Woodrow Wilson|7
1983-11-04|Cox|12|Booker T. Washington|7
1983-11-04|Great Bridge|43|Churchland|0
1983-11-04|Indian River|6|Manor|6
1983-11-04|Kellam|14|Granby|13
1983-11-04|Kempsville|23|Green Run|6
1983-11-04|Lake Taylor|30|Princess Anne|0
1983-11-04|Maury|23|First Colonial|17
1983-11-04|Norcom|14|Western Branch|14
1983-11-04|Norview|10|Bayside|6
1983-11-04|Oscar Smith|18|Deep Creek|0
1983-11-04|Woodrow Wilson|6|Cradock|3
1983-11-11|Bayside|26|Princess Anne|13
1983-11-11|Cox|14|Western Branch|0
1983-11-11|Deep Creek|12|Manor|6
1983-11-11|Granby|17|Maury|16
1983-11-11|Great Bridge|32|Woodrow Wilson|20
1983-11-11|Hampton|27|Bethel|0
1983-11-11|Kellam|20|Booker T. Washington|15
1983-11-11|Kempsville|34|Norview|7
1983-11-11|Lake Taylor|14|First Colonial|9
1983-11-11|Norcom|13|Cradock|8
1983-11-11|Oscar Smith|3|Indian River|0
1983-11-11|Warwick|38|Churchland|13
1983-11-18|Hopewell|20|Patrick Henry(A)|6
1983-11-18|Jefferson/Huguenot/Wythe|14|Marshall-Walker|0
1983-11-18|Great Bridge|17|Hampton|14
1983-11-18|Kempsville|24|Lake Taylor|10
1983-11-18|South Lakes|24|J.E.B. Stuart|6
1983-11-18|Mount Vernon|17|T.C. Williams|8
1983-11-18|Pulaski County|16|Stonewall Jackson|12
1983-11-18|George Washington|20|Northside|7
1983-11-18|L.C. Bird|28|Southampton|7
1983-11-18|Nottoway|10|Tabb|6
1983-11-18|Courtland|23|Park View(S)|6
1983-11-18|Harrisonburg|14|Culpepper|6
1983-11-18|Laurel Park|21|William Campbell|6
1983-11-18|Covington|12|Brookville|7
1983-11-18|George Wythe(W)|14|Tazewell|0
1983-11-18|King William|21|Onancock|6
1983-11-18|Surry County|7|Washington & Lee|0
1983-11-18|Madison County|9|Clarke County|3
1983-11-18|Central Senior|47|Riverheads|6
1983-11-18|Holston|13|Independence|6
1983-11-18|Parry McCluer|33|Drewry Mason|14
1983-11-18|Pennington|33|Garden|3
1983-11-18|Giles|27|Abingdon|0
1983-11-25|Hopewell|20|Jefferson/Huguenot/Wythe|7
1983-11-25|Kempsville|27|Great Bridge|21
1983-11-25|Mount Vernon|49|South Lakes|0
1983-11-25|Pulaski County|17|George Washington|7
1983-11-25|L.C. Bird|21|Nottoway|10
1983-11-25|Courtland|45|Harrisonburg|6
1983-11-25|Laurel Park|15|Covington|14
1983-11-25|George Wythe(W)|7|Giles|0
1983-11-25|Surry County|33|King William|12
1983-11-25|Madison County|14|Central Senior|13
1983-11-25|Parry McCluer|35|Holston|0
1983-11-25|Powell Valley|50|Pennington|28
1983-12-03|Kempsville|13|Hopewell|6
1983-12-03|Mount Vernon|28|Pulaski County|21
1983-12-03|Courtland|24|L.C. Bird|23
1983-12-03|George Wythe(W)|21|Laurel Park|8
1983-12-03|Madison County|41|Surry County|15
1983-12-03|Parry McCluer|7|Powell Valley|3
1983-12-10|Mount Vernon|10|Kempsville|0
1983-12-10|Courtland|14|George Wythe(W)|7
1983-12-10|Parry McCluer|21|Madison County|6